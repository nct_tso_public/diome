# Scripts to perform fan shape correction for OCT images
# 2023 Peng Liu Email: firstname.lastname@nct-dresden.de

from funcs import fan_shape_correction as fsc
import os
import nrrd
import numpy as np
import yaml



def reshaper(
        img_path: str,
        yaml_path: str,
        output_path: str = None,
):
    img, header = nrrd.read(img_path)
    img = np.transpose(img, (1, 0, 2))

    with open(yaml_path, 'r') as f:
        oct_meta = yaml.safe_load(f,)

    oct = oct_meta["oct_settings"]
    print(oct_meta.keys())

    img_full = np.zeros((1024, 560, 560), dtype=np.uint8)

    A_scan_l, A_scan_r = oct['Ascan_used']["min"], oct['Ascan_used']["max"]
    B_scan_l, B_scan_r = oct["Bscan_used"]["min"], oct["Bscan_used"]["max"]

    # Copy 'img' into the specified region of 'img_full'
    img_full[:, A_scan_l: A_scan_r + 1, B_scan_l: B_scan_r + 1] = img

    img_full_corrected = fsc.FanDistCorr(
        Int=img_full,
        oct=oct,
        method='nearest',
    )
    img_full_corrected = np.transpose(img_full_corrected, (1, 0, 2))
    if img_full_corrected is not None :
        if output_path is None:
            output_path = os.path.join(os.path.dirname(img_path), "corrected.nrrd")
        nrrd.write(
            output_path, 
            img_full_corrected, 
            header={
                'spacings': 
                    [
                        oct['Distortion_Correction_Preview']['DC_OUT_dZ_mm'],
                        oct['Distortion_Correction_Preview']['DC_OUT_dX_mm'],
                        oct['Distortion_Correction_Preview']['DC_OUT_dY_mm']
                    ],
                    'origin': [0, 0, 0],
                    'type': 'uint8',
                }
            )
        print("saved to:", output_path)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Script to correct fan shape distortion")
    parser.add_argument("--img_path", type=str, required=True)
    parser.add_argument("--yaml_path", type=str, required=True)
    parser.add_argument("--output_path", type=str, default=None)

    args = parser.parse_args()

    img_path = args.img_path
    yaml_path = args.yaml_path
    output_path = args.output_path

    reshaper(
        img_path=img_path,
        yaml_path=yaml_path,
        output_path=output_path,
    )








