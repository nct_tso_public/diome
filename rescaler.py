# Scripts to modify the spacing of OCT images/labels and rescale sparse landmarks
# 2023 Peng Liu Email: firstname.lastname@nct-dresden.de

import os
import nrrd
import numpy as np
import json
import pickle
import argparse
import glob


with open("funcs/header_original_scale.pkl", 'rb') as f:
    header_original_scale = pickle.load(f)
    f.close()
with open("funcs/header_real_scale.pkl", 'rb') as f:
    header_real_scale = pickle.load(f)
    f.close()

spacings_real_scale = header_real_scale["space directions"]
spacings_original_scale = header_original_scale["space directions"]

spacings_real_scale = np.array([spacings_real_scale[0,0], spacings_real_scale[1,1], spacings_real_scale[2,2]])
spacings_original_scale = np.array([spacings_original_scale[0,0], spacings_original_scale[1,1], spacings_original_scale[2,2]])

print("spacings_real_scale: ", spacings_real_scale)
print("spacings_original_scale: ", spacings_original_scale)



def update_spacing(
    filepath,
    output_folder=None,
    target_spacings=spacings_real_scale,
):
    image, header = nrrd.read(filepath)
    print("old spacings:", header["space directions"])
    header["space directions"] = target_spacings

    output_path = os.path.join(output_folder, os.path.basename(filepath))
    nrrd.write(
        output_path,
        image,
        header=header,
    )
    print("saved to:", output_path)


landmarks_filename_list = [
    "annulus.json",
    "long_process_of_incus.json",  
    "malleus_handle.json",  
    "short_process_of_malleus.json",  
    "stapes.json",  
    "umbo.json",
]

def scale_landmarks(
    landmarks_folder,
    output_folder=None,
    source_spacings=spacings_original_scale,
    target_spacings=spacings_real_scale,
):
    """Rescale landmarks to match the spacing of the OCT images

    Args:
        landmarks_folder (str): folder where the .json files of landmarks are stored
        output_folder (str): folder where the rescaled .json files of landmarks will be stored
        source_spacings (str): spacings of the input landmarks
        target_spacings (str): spacings of the target landmarks
    """
    landmarks_coord_list = []
    landmarks_coord_scaled_list = []
    landmarks_json_scaled_list = []
    for idx, landmark_filename in enumerate(landmarks_filename_list):
        landmark_filepath = os.path.join(landmarks_folder, landmark_filename)
        if os.path.exists(landmark_filepath):
            print("File exists: ", landmark_filepath)
            with open(landmark_filepath, 'r') as f:
                landmarks_json = json.load(f)
                # landmarks_json_list.append(json.load(f))
            landmarks_points = [ c["position"]  for c in landmarks_json["markups"][0]["controlPoints"]]
            print(len(landmarks_points))
            landmarks_coord_list.append(landmarks_points)
            landmarks_points = np.array(landmarks_points)

            landmarks_scaled = landmarks_points * target_spacings / source_spacings
            landmarks_coord_scaled_list.append(landmarks_scaled.tolist())

            for idx_c, coord in enumerate(landmarks_scaled):
                # print("before:", landmarks_points[idx_c], "after: ", coord)
                landmarks_json["markups"][0]["controlPoints"][idx_c]["position"] = coord.tolist()
            landmarks_json_scaled_list.append(landmarks_json)

            if output_folder is not None:
                output_path = os.path.join(output_folder, landmark_filename)
                with open(output_path, 'w') as f:
                    json.dump(landmarks_json, f)
                    print("Write to: ", output_path)
    return landmarks_coord_scaled_list, landmarks_json_scaled_list



if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_folder', type=str, required=True, help="folder where the .nrrd images or .json landmarks are stored")
    parser.add_argument("--target_folder", type=str, required=True, help="folder where the rescaled images/landmarks will be stored")
    parser.add_argument("--images", action="store_true", default=False)
    parser.add_argument("--landmarks", action="store_true", default=False)
    parser.add_argument("--target_type", type=str, default="real", help="real or original")
    args = parser.parse_args()
    data_folder = args.data_folder
    target_folder = args.target_folder
    images = args.images
    landmarks = args.landmarks
    target_type = args.target_type


    if target_type == "real":
        source_spacings = spacings_original_scale
        target_spacings = spacings_real_scale
    elif target_type == "original":
        source_spacings = spacings_real_scale
        target_spacings = spacings_original_scale

    if images:
        image_path_list = glob.glob(os.path.join(data_folder, "*.nrrd"))
        for idx, image_path in enumerate(image_path_list):
            print("====================Processing sample: ", idx)
            update_spacing(
                filepath=image_path,
                output_folder=target_folder,
                target_spacings=target_spacings,
            )

    if landmarks:
        scale_landmarks(
            landmarks_folder=data_folder,
            output_folder = target_folder,
            source_spacings=source_spacings,
            target_spacings=target_spacings,
        )

    # example of updating all the landmarks in DIOME folder
    # for idx in range(0, 43):
    #     print("====================Processing sample: ", idx)

    #     cur_folder = os.path.join(data_folder, "sample_{}/annotations/merged/landmarks/".format(idx))
    #     output_folder = os.path.join(target_folder, "sample_{}/annotations/merged/landmarks/".format(idx))
    #     if idx >= 38:
    #         cur_folder = os.path.join(data_folder, "sample_{}/annotations/landmarks/".format(idx))
    #         output_folder = os.path.join(target_folder, "sample_{}/annotations/landmarks/".format(idx))
        
    #     scale_landmarks(
    #         landmarks_folder=cur_folder,
    #         output_folder = output_folder,
    #         source_spacings=spacings_original_scale,
    #         target_spacings=spacings_real_scale,
    #     )





