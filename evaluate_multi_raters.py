import os
from funcs import evaluate
import numpy as np
import glob
import nrrd
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_folder', type=str, required=True)
    args = parser.parse_args()
    folder = args.data_folder

    f1_score_list = []
    hd_list = []

    for idx in range(0, 42):
        print("=========== sample:", idx)
        sample_folder = os.path.join(folder, "sample_{}".format(idx))
        anno_folder = os.path.join(sample_folder, "annotations")
        anno_path_list = [ os.path.join(anno_folder, "seg_{}.seg.nrrd".format(rater)) for rater in range(0, 3)]
        merged_path = os.path.join(anno_folder, "merged", "seg_merged.seg.nrrd")

        f1_score_sample_list = []
        hd_sample_list = []

        gt_data, _ = nrrd.read(merged_path)
        for idx_rater in range(3):
            rater_data, _ = nrrd.read(anno_path_list[idx_rater])
            f1 = evaluate.calc_f1_score_pytorch(
                gt = gt_data,
                pred = rater_data,
            )
            f1_score_sample_list.append(f1)

            hd = evaluate.calc_hausdorff_distance(
                gt=gt_data, 
                pred=rater_data,
            )
            hd_sample_list.append(hd)
        f1_score_list.append(f1_score_sample_list)
        hd_list.append(hd_sample_list)

    f1_score_list = np.array(f1_score_list)
    hd_list = np.array(hd_list)

    mean_f1_score = np.mean(f1_score_list, axis=0)
    mean_hd = np.mean(hd_list, axis=0)

    print("mean f1 score of 3 raters:", mean_f1_score)
    print("mean hd of 3 raters:", mean_hd)

