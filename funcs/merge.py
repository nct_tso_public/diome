import numpy as np
import SimpleITK as sitk
import nrrd
import os
import funcs.visualize as visualize
from funcs.utils import timeit, NumpyEncoder
import json
import sys
import glob
import pickle


def get_template_header():
    with open("funcs/template_header.pkl", "rb") as f:
        header = pickle.load(f)

    return header


@timeit
def merge_via_staple(segments):
    """
        STAPLE algorithm for merging segmentation results
    """
    
    print("before merging:")
    for s in segments:
        print(np.sum(s))

    segmentations_3d = segments
    # if not isinstance(segmentations_3d[0], sitk.Image):
    if isinstance(segmentations_3d[0], np.ndarray):
        segmentations_3d = [sitk.GetImageFromArray(seg) for seg in segmentations_3d]

    print(len(segmentations_3d), "to merge")

    # Perform STAPLE to estimate consensus 3D segmentation
    staple_filter = sitk.STAPLEImageFilter()
    staple_image = staple_filter.Execute(segmentations_3d)

    consensus_array = sitk.GetArrayFromImage(staple_image)
    consensus_array = (consensus_array > 0.9).astype(np.uint8)

    print("Consensus 3D Segmentation:", consensus_array.shape)
    print("after merging:" , np.sum(consensus_array))
    return consensus_array


@timeit
def merge_via_majority_voting_simpleitk(segments):
    """Merge the input segmentations via majority voting

    Args:
        segments (list): a list of segmentations, each segmentation is a 3D numpy array

    Returns:
        consensus_array (np.ndarray): the merged segmentation
    """
    print("before merging:")
    for s in segments:
        print(np.sum(s))

    segmentations_3d = segments
    # if not isinstance(segmentations_3d[0], sitk.Image):
    if isinstance(segmentations_3d[0], np.ndarray):
        segmentations_3d = [sitk.GetImageFromArray(seg) for seg in segmentations_3d]

    # print(len(segmentations_3d))
    # Perform majority voting to estimate consensus 3D segmentation
    consensus_segmentation_3d = sitk.LabelVoting(segmentations_3d, 0)

    consensus_array = sitk.GetArrayFromImage(consensus_segmentation_3d)

    print("Consensus 3D Segmentation:", consensus_array.shape)
    # print(consensus_array)
    print("after merging:" , np.sum(consensus_array))
    return consensus_array


segmentation_labels = {
    # "background": 0,
    "tympanic_membrane": 1,
    "malleus": 2,
    "incus": 3,
    "stapes": 4,
    "promontory": 5,
}


@timeit
def merge(
    img1_path,
    img2_path,
    img3_path,
    output_folder = None,
    # output_path = None,
    # save_single_seg = False,
    merge_method = "STAPLE",
):

    """Main functions for mering segmentation results

    Args:
        img1_path: path to the first segmentation result
        img2_path: path to the second segmentation result
        img3_path: path to the third segmentation result
        output_folder: path to the output folder
        merge_method: "STAPLE" or "majority_voting"

    Returns:
        merged_list: a list of merged segmentation results
        output_path_list: a list of paths to the merged segmentation results
    """

    img1, _ = nrrd.read(img1_path)
    img2, _ = nrrd.read(img2_path)
    img3, _ = nrrd.read(img3_path)

    # with open("./template_header.pkl", "rb") as f:
    #     header = pickle.load(f)

    header = get_template_header()
    print(img1.shape, img2.shape, img3.shape)

    merged_list = []
    output_path_list = []

    for (k, v) in segmentation_labels.items():
        print("----------------------\n", "Prcessing: ", k,)
        img1_structure = np.asarray(img1 == v, dtype=np.uint8)
        img2_structure = np.asarray(img2 == v, dtype=np.uint8)
        img3_structure = np.asarray(img3 == v, dtype=np.uint8)

        if merge_method == "STAPLE":
            consensus_array = merge_via_staple(
                segments=[
                    img1_structure, 
                    img2_structure, 
                    img3_structure,
                ])
        elif merge_method == "majority_voting":
            consensus_array = merge_via_majority_voting_simpleitk(
                segments=[
                    img1_structure, 
                    img2_structure, 
                    img3_structure,
                ])
        
        print(np.unique(consensus_array))

        if output_folder:
        # if output_path:
            output_path = os.path.join(output_folder, "test_STAPLE_{}.seg.nrrd".format(k))
            output_path_list.append(output_path)
            nrrd.write(output_path, consensus_array, header, )
            print("saved to: ", output_path)
        merged_list.append(consensus_array)

    return merged_list, output_path_list



def merge_results(
        file_path_list,
        output_path = None,
):
    img_merged = None
    for idx, f in enumerate(file_path_list):
        print(idx, f)
        img, _ = nrrd.read(f)
        img = np.asarray(img == 1, dtype=np.uint8) * (idx + 1)
        if img_merged is None:
            img_merged = img
        else:
            img_merged = img_merged + img

    print(np.unique(img_merged))

    with open("./template_header.pkl", "rb") as f:
        header = pickle.load(f)

    if output_path:
        nrrd.write(output_path, img_merged, header=header)
        print("saved to: ", output_path)






if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--seg1_path", type=str, )
    parser.add_argument("--seg2_path", type=str, )
    parser.add_argument("--seg3_path", type=str, )
    parser.add_argument("--output_folder", type=str, default="None")
    args = parser.parse_args()

    merged_list, output_path_list = merge(
        img1_path = args.seg1_path,
        img2_path = args.seg2_path,
        img3_path = args.seg3_path,
        output_folder = args.output_folder,
    )

    # for idx in range(10):
    #     print(f"idx: {idx}")
    #     merge_via_staple()

    # test_staple_1()
    # majority_voting_simpleitk()
    # _calc_total_time()

    # merged_list = test_staple_simpleitk(
    #     output_folder="test_data",
    # )

    # file_path_list = ["test_data/test_STAPLE_{}.seg.nrrd".format(k) for k in segmentation_labels.keys()]

    # for f in file_path_list:
    #     visualize.visualize_seg_animation(
    #         path = f,
    #     )

    merge_results( 
        file_path_list=output_path_list,
        output_path = "test_data/test_STAPLE_merged.seg.nrrd",
    )

