import numpy as np
import json



def get_current_time():
    """Get current time in the format of %Y-%m-%d_%H-%M-%S

    Returns:
        time: current time
    """
    import datetime
    return datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

def timeit(func):
    import time
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        print(f"{get_current_time()} Time elapsed: {func.__name__} {time.time() - start}")
        return result
    return wrapper


class NumpyEncoder(json.JSONEncoder):
    """
    Convert numpy array to list for json serialization
    """
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
    

def xmltodict_postproc(path, key, value):
    if isinstance(value, str):
        if value.isdigit():
            try:
                return key, int(value)
            except (ValueError, TypeError):
                return key, value
        else:
            try:
                return key, float(value)
            except (ValueError, TypeError):
                return key, value
    return key, value