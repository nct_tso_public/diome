import nrrd
import matplotlib.pyplot as plt
import numpy as np
import os


def visualize_image(
        img_path,
        img_idx=200,
        axis=2,
        show_axis=True,
        show=True,
        output_path=None,
        flip=False,
        default_spacing=False,
    ):
    """
    Visualize a slice of a 3D image stored in an NRRD file.

    This function reads a 3D image from an NRRD file, extracts a slice along a
    specified axis, and displays the slice using Matplotlib.

    Parameters:
    -----------
    img_path : str
        The path to the NRRD file containing the 3D image.

    img_idx : int, optional
        The index of the slice to visualize along the specified axis.
        Default is 200.

    axis : int, optional
        The axis along which to extract the slice (0 for X, 1 for Y, 2 for Z).
        Default is 2 (Z-axis).

    show_axis : bool, optional
        Whether to display axis ticks and labels on the plot. Default is True.

    show : bool, optional
        Whether to display the image. If False, the image is only saved to the
        specified output path (if provided). Default is True.

    output_path : str or None, optional
        The path to save the image to. If None, the image is not saved.
        Default is None.

    flip : bool, optional
        Whether to flip and rotate the slice for proper orientation. This is
        useful when visualizing images in some coordinate systems.
        Default is False.

    default_spacing : bool, optional
        Whether to use default voxel spacings (0.019, 0.0078125, 0.019) when
        extracting physical dimensions. If False, it uses spacings from the NRRD
        file header if available.
        Default is False.

    """
    img, header = nrrd.read(img_path)

    if default_spacing:
        spacing = [0.019, 0.0078125, 0.019]
    else:
        if "space directions" in header.keys():
            spacing = [
                header["space directions"][0,0],
                header["space directions"][1,1],
                header["space directions"][2,2],
            ]
        else:
            spacing = header["spacings"]

    num_slices = img.shape[axis]

    fig, axs = plt.subplots(1, 1, figsize=(10, 10))
    physical_dims = np.multiply(spacing, img.shape)
    current_slice = img_idx

    if axis == 0:
        slice_data = img[current_slice, ...]
        extent = [0, physical_dims[2], 0, physical_dims[1]]
    elif axis == 1:
        slice_data = img[:, current_slice, ...]
        extent = [0, physical_dims[2], 0, physical_dims[0]]
    elif axis == 2:
        slice_data = img[:, :, current_slice]
        extent = [0, physical_dims[1], 0, physical_dims[0]]
        if flip:
            slice_data = np.flip(slice_data, axis=0)
            slice_data = np.rot90(slice_data, k=3)
            extent = [0, physical_dims[0], 0, physical_dims[1]]

    # Display the slice with appropriate extent
    axs.imshow(slice_data, cmap="gray", extent=extent)
    # axs.imshow(slice_data, cmap='gray')
    if show_axis:
        axs.axis("on")
    else:
        axs.axis("off")

    if output_path is not None:
        plt.savefig(output_path, bbox_inches='tight', pad_inches = 0)
        print("saved to: ", output_path)

    if show:
        plt.tight_layout()
        plt.show()


label_colors = {
    0: (0, 0, 0),     # Black for 0
    1: (255, 0, 0),   # Red for 1
    2: (0, 0, 255),   # Blue for 2
    3: (0, 255, 0),   # Green for 3
    4: (255, 255, 0),  # Yellow for 4
    5: (255, 0, 255),  # Purple for 5
}
    

def visualize_segmentation(
        seg_path,
        img_idx=200,
        show_axis=True,
        show=True,
        output_path=None,
        default_spacing=False,
        ):
    """
    Visualize a segmentation mask stored in an NRRD file.

    This function reads a 3D segmentation mask from an NRRD file, extracts a
    2D slice from the specified index, and visualizes it with labeled colors.

    Parameters:
    -----------
    seg_path : str
        The path to the NRRD file containing the 3D segmentation mask.

    img_idx : int, optional
        The index of the 2D slice to visualize. Default is 200.

    show_axis : bool, optional
        Whether to display axis ticks and labels on the plot. Default is True.

    show : bool, optional
        Whether to display the segmented image. If False, the image is only saved
        to the specified output path (if provided). Default is True.

    output_path : str or None, optional
        The path to save the segmented image to. If None, the image is not saved.
        Default is None.

    default_spacing : bool, optional
        Whether to use default voxel spacings (0.019, 0.0078125, 0.019) when
        extracting physical dimensions. If False, it uses spacings from the NRRD
        file header if available.
        Default is False.
    """

    img, header = nrrd.read(seg_path)

    if default_spacing:
        spacing = [0.019, 0.0078125, 0.019]
    else:
        if "space directions" in header.keys():
            spacing = [
                header["space directions"][0, 0],
                header["space directions"][1, 1],
                header["space directions"][2, 2],
            ]
        else:
            spacing = header["spacings"]

    axis = 2
    img_idx = img_idx
    num_slices = img.shape[axis]

    fig, axs = plt.subplots(1, 1, figsize=(10, 10))

    physical_dims = np.multiply(spacing, img.shape)

    current_slice = int(img_idx * (img.shape[axis] / num_slices))

    slice_data = img[:, :, current_slice]
    slice_data = np.flip(slice_data, axis=0)
    slice_data = np.rot90(slice_data, k=3)

    colored_mask = np.zeros((img.shape[1], img.shape[0], 3), dtype=np.uint8)

    for label, color in label_colors.items():
        mask = slice_data == label
        colored_mask[mask] = color

    # Display the slice with appropriate extent
    axs.imshow(colored_mask, extent=[0, physical_dims[0], 0, physical_dims[1]])
    # axs.imshow(slice_data, cmap='gray')
    if show_axis:
        axs.axis("on")
    else:
        axs.axis("off")

    if output_path is not None:
        plt.savefig(output_path, bbox_inches='tight', pad_inches = 0)
        print("saved to: ", output_path)

    if show:
        plt.tight_layout()
        plt.show()


def visualize_segmentation_multiple(
        seg_path_list,
        img_idx=200,
        show_axis=True,
        show=True,
        output_path=None,
        default_spacing=False,
        ):
    """
    Visualize multiple segmentation masks stored in NRRD files simultaneously.

    This function reads multiple 3D segmentation masks from a list of NRRD files,
    extracts a 2D slice from each mask at the specified index, and visualizes them
    side by side with labeled colors.

    Parameters:
    -----------
    seg_path_list : list of str
       A list of file paths to the NRRD files containing the 3D segmentation masks.

    img_idx : int, optional
       The index of the 2D slice to visualize from each segmentation mask.
       Default is 200.

    show_axis : bool, optional
       Whether to display axis ticks and labels on the plot. Default is True.

    show : bool, optional
       Whether to display the segmented images. If False, the images are only saved
       to the specified output path (if provided). Default is True.

    output_path : str or None, optional
       The path to save the combined segmented images to. If None, the images are not
       saved. Default is None.

    default_spacing : bool, optional
       Whether to use default voxel spacings (0.019, 0.0078125, 0.019) when extracting
       physical dimensions. If False, it uses spacings from the NRRD file header if
       available.
       Default is False.
    """
    img_list = []

    for seg_path in seg_path_list:
        img, header = nrrd.read(seg_path)
        img_list.append(img)

    if default_spacing:
        spacing = [0.019, 0.0078125, 0.019]
    else:
        if "space directions" in header.keys():
            spacing = [
                header["space directions"][0, 0],
                header["space directions"][1, 1],
                header["space directions"][2, 2],
            ]
        else:
            spacing = header["spacings"]

    axis = 2
    img_idx = img_idx
    num_slices = img.shape[axis]

    physical_dims = np.multiply(spacing, img.shape)

    slice_data_list = []
    colored_mask_list = []
    for img in img_list:
        current_slice = int(img_idx * (img.shape[axis] / num_slices))
        slice_data = img[:, :, current_slice]
        slice_data = np.flip(slice_data, axis=0)
        slice_data = np.rot90(slice_data, k=3)
        slice_data_list.append(slice_data)

        colored_mask = np.zeros((img.shape[1], img.shape[0], 3), dtype=np.uint8)
        for label, color in label_colors.items():
            mask = slice_data == label
            colored_mask[mask] = color

        colored_mask_list.append(colored_mask)

    fig, axs = plt.subplots(1, len(colored_mask_list), figsize=(10 * len(colored_mask_list), 10 ))
    for idx_c, colored_mask in enumerate(colored_mask_list):
        # Display the slice with appropriate extent
        axs[idx_c].imshow(colored_mask, extent=[0, physical_dims[0], 0, physical_dims[1]])
        if show_axis:
            axs[idx_c].axis("on")
        else:
            axs[idx_c].axis("off")

    if output_path is not None:
        plt.savefig(output_path, bbox_inches='tight', pad_inches = 0)
        print("saved to: ", output_path)

    if show:
        plt.tight_layout()
        plt.show()



def visualize_animation(
        path,
        axis=2, 
        step=10,
        default_spacing=False,
        output_folder=None,
        gif=True,
    ):
    """
    Visualize a 3D image as an animation of 2D slices along a specified axis.

    This function reads a 3D image from an NRRD file, extracts and visualizes 2D slices
    along a specified axis as an animation. It allows for saving individual frames as
    images and generating a GIF animation.

    Parameters:
    -----------
    path : str
        The path to the NRRD file containing the 3D image.

    axis : int, optional
        The axis along which to extract and visualize 2D slices (0 for X, 1 for Y, 2 for Z).
        Default is 2 (Z-axis).

    step : int, optional
        The step size for iterating through slices in the animation. Default is 10.

    default_spacing : bool, optional
        Whether to use default voxel spacings (0.019, 0.0078125, 0.019) when extracting
        physical dimensions. If False, it uses spacings from the NRRD file header if
        available.
        Default is False.

    output_folder : str or None, optional
        The folder path to save individual frames of the animation as PNG images. If None,
        individual frames are not saved. Default is None.

    gif : bool, optional
        Whether to generate a GIF animation from the saved frames. Default is True.

    """
    # show scans one by one
    data, header = nrrd.read(path)
    data = np.transpose(data, (1, 0, 2))
    num = 1
    im = None
    if default_spacing:
        spacing = [0.019, 0.0078125, 0.019]
    else:
        if "space directions" in header.keys():
            spacing = [
                header["space directions"][0,0],
                header["space directions"][1,1],
                header["space directions"][2,2],
            ]
        else:
            spacing = header["spacings"]
    physical_dims = np.multiply(spacing, data.shape)

    num_slices = data.shape[axis]
    for i in range(0, num_slices, step):
        current_slice = i
        if axis == 0:
            image = data[current_slice, ...]
            extent = [0, physical_dims[2], 0, physical_dims[1]]
        elif axis == 1:
            image = data[:, current_slice, ...]
            extent = [0, physical_dims[2], 0, physical_dims[0]]
        elif axis == 2:
            image = data[:, :, current_slice]
            extent = [0, physical_dims[1], 0, physical_dims[0]]
        if not im:
            im = plt.imshow(image, cmap='gray', vmin=0, vmax=np.max(data), extent=extent)
        else:
            im.set_data(image)
        plt.pause(.1)
        plt.draw()
        if output_folder is not None:
            plt.savefig(os.path.join(output_folder, "{}.png".format(i)), bbox_inches='tight', pad_inches = 0)
            print("saved to: ", "test_data/{}.png".format(i))

    if output_folder is not None and gif:
        import imageio
        images = []
        idx = 0
        while os.path.exists(os.path.join(output_folder, "{}.png".format(idx))):
            filename = os.path.join(output_folder, "{}.png".format(idx))
            images.append(imageio.imread(filename))
            idx += step
        imageio.mimsave(os.path.join(output_folder, "animation.gif"), images)
        print("saved to: ", os.path.join(output_folder, "animation.gif"))



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--img_path", type=str,)
    parser.add_argument("--axis", type=int, default=2)
    parser.add_argument("--img_idx", type=int, default=200)

    args = parser.parse_args()
    img_path = args.img_path
    img_idx = args.img_idx
    axis = args.axis

    # visualize_image(
    #     img_path=img_path,
    #     axis=axis,
    #     img_idx=img_idx,
    #     show=True,
    #     # show_axis=False,
    #     # output_path=os.path.join("test_data", "{}.png".format(os.path.basename(img_path).split(".")[0]))
    #     flip=False,
    #     )


    # visualize_segmentation(
    #     seg_path = img_path,
    #     img_idx = img_idx,
    #     show_axis=False,
    #     show=False,
    #     output_path=os.path.join("test_data", "{}.png".format(os.path.basename(img_path).split(".")[0])),
    # )

    # visualize_segmentation_multiple(
    #     seg_path_list=[img_path_2, img_path_0, img_path_1],
    #     img_idx = img_idx,
    #     show_axis=False,
    # )

    visualize_animation(
        path=img_path,
        axis=axis,
        step=3,
        default_spacing=False,
        output_folder="test_data/fan_shape_correction",
        gif=True,
        )

