import numpy as np
import nrrd
import os
import funcs.utils as utils
import vtk
import glob
import json



label_name = [
    "1_Tympanic_Membrane",
    "2_Malleus",
    "3_Incus",
    "4_Stapes",
    "5_Promontory",
]


@utils.decorator_timer
def convert_vtk(label_map_path, output_folder=None):
    # Step 1: Load the label map and create VTK image data
    label_map, label_map_header = nrrd.read(label_map_path)
    label_map = np.transpose(label_map, axes=(2, 1, 0))  # Transpose to match VTK's ordering

    # Get the dimensions of the label map
    dims = label_map.shape
    spacing = label_map_header['space directions']

    label_list = np.unique(label_map)

    for idx_s in label_list:
        if idx_s == 0:
            continue
        print("Converting label", idx_s)
        label = (label_map == idx_s).astype(np.uint8)

        # Create a VTK image data
        image_data = vtk.vtkImageData()
        image_data.SetDimensions(dims[0], dims[1], dims[2])
        # image_data.SetSpacing([spacing[0,0], spacing[1,1], spacing[2,2]])
        image_data.SetSpacing([0.019, 0.0078125, 0.019])
        image_data.AllocateScalars(vtk.VTK_UNSIGNED_CHAR, 1)  # We use an unsigned char array for binary data

        # Copy the label map data into the VTK image data

        flat_label_map = label.flatten()

        # Convert the flattened label map to a VTK unsigned char array
        vtk_data_array = vtk.vtkUnsignedCharArray()
        vtk_data_array.SetNumberOfTuples(len(flat_label_map))
        vtk_data_array.SetVoidArray(flat_label_map, len(flat_label_map), 1)

        # Set the VTK data array as the scalars for the VTK image data
        image_data.GetPointData().SetScalars(vtk_data_array)

        # Step 3: Extract the surface using the Marching Cubes algorithm
        marching_cubes = vtk.vtkMarchingCubes()
        marching_cubes.SetInputData(image_data)
        marching_cubes.SetValue(0, 1.0)  # Extract the isosurface at the value 1.0 (structure)

        if output_folder:
            # Step 4: Save the mesh to an STL file
            output_path = os.path.join(output_folder, "{}.stl".format(label_name[idx_s - 1]))
            stl_writer = vtk.vtkSTLWriter()
            stl_writer.SetFileName(output_path)
            stl_writer.SetInputConnection(marching_cubes.GetOutputPort())
            stl_writer.Write()
            print("saved to:", output_path)
