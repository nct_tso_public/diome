import numpy as np
import argparse
from sklearn.metrics import f1_score
import nrrd
import os, sys
import json
from scipy.spatial.distance import directed_hausdorff, euclidean
import torch
from torcheval.metrics.functional import multiclass_f1_score, binary_f1_score
import glob
# from funcs.utils import timeit
try:
    from funcs import utils
except:
    import utils

segmentation_labels = {
    # "background": 0,
    "tympanic_membrane": 1,
    "malleus": 2,
    "incus": 3,
    "stapes": 4,
    "promontory": 5,
}


def calc_f1_score(gt, pred):
    """Calculate f1 score for each structure using sklearn.metrics.f1_score
        Run on CPU so can be a bit slow
    Args:
        gt (numpy.ndarray): ground truth segmentation
        pred (numpy.ndarray): predicted segmentation

    Returns:
        f1_score_list (list): list of f1 scores for each structure
        avg (float): average f1 score
    """
    gt = gt.flatten()
    pred = pred.flatten()

    f1_score_list = []

    for idx, seg in enumerate(segmentation_labels.keys()):
        print("idx", idx)
        f = f1_score(gt==segmentation_labels[seg], pred==segmentation_labels[seg])
        print("f1 score {}:".format(seg), f)
        f1_score_list.append(f)

    avg = np.mean(f1_score_list)
    print("avg f1 score:", avg)
    return f1_score_list, avg


@utils.timeit
def calc_f1_score_pytorch(gt,pred,):
    """Calculate f1 score for each structure using torcheval.metrics.functional.multiclass_f1_score
    
    Args:
        gt (numpy.ndarray): ground truth segmentation
        pred (numpy.ndarray): predicted segmentation
    
    Returns:
        f1_score_list (list): list of f1 scores for each structure
    
    """
    if len(gt.shape) > 0:
        gt = gt.flatten()
        pred = pred.flatten()
    gt = torch.from_numpy(gt)
    pred = torch.from_numpy(pred)
    gt.to("cuda")
    pred.to("cuda")
    print("gt.shape", gt.shape)
    print("pred.shape", pred.shape)

    f1_score_list = []
    for label in range(1, 6):
        pred_structure = torch.where(pred == label, 1, 0)
        gt_structure = torch.where(gt == label, 1, 0)
        if not pred_structure.any() and not gt_structure.any():
            # f1_score_list.append(0.0)
            f1_score_list.append(1.0)
            print("label", label, "f1 score:", 1.0)
        else:
            f1_score = binary_f1_score(pred_structure, gt_structure)
            f1_score_list.append(f1_score.item())
            print("label", label, "f1 score:", f1_score)

    print("f1_score_list", f1_score_list)
    return f1_score_list



@utils.timeit
def calc_hausdorff_distance(
    gt, 
    pred,
    size=100000,    
):
    """Calculate hausdorff distance for each structure using scipy.spatial.distance.directed_hausdorff
    
    Args:
        gt (numpy.ndarray): ground truth segmentation
        pred (numpy.ndarray): predicted segmentation
        size (int, optional): size of the output array. Defaults to 100000.

    Returns:
        hausdorff_distance_list (list): list of hausdorff distances for each structure
        avg (float): average hausdorff distance
    """
    gt = torch.from_numpy(gt)
    pred = torch.from_numpy(pred)
    gt.to("cuda")
    pred.to("cuda")
    hausdorff_distance_list = []
    spacings = torch.tensor([0.019, 0.0078125, 0.019])
    for idx, seg in enumerate(segmentation_labels.keys()):
        pred_idx = torch.nonzero(pred == idx, as_tuple=False)
        gt_idx = torch.nonzero(gt == idx, as_tuple=False)

        if not pred_idx.any() or not gt.any():
            print("no points in seg", seg)
            hausdorff_distance_list.append(0.0)
            continue
        if pred_idx.shape[0] == 0 or gt_idx.shape[0] == 0:
            print("no points in seg", seg)
            hausdorff_distance_list.append(0.0)
            continue
        # if the number of points is larger than size, downsample
        if pred_idx.shape[0] > size:
            pred_idx = torch.nn.functional.interpolate(pred_idx.float().unsqueeze(0).unsqueeze(0),
                                                                size=(size, 3)).squeeze()
        if gt_idx.shape[0] > size:
            gt_idx = torch.nn.functional.interpolate(gt_idx.float().unsqueeze(0).unsqueeze(0),
                                                                size=(size, 3)).squeeze()
            
        print(pred_idx.shape, gt_idx.shape)
        hd_pred_to_gt, idx_pred_in_p2g, idx_gt_in_p2g = directed_hausdorff(pred_idx, gt_idx)
        hd_gt_to_pred, idx_gt_in_g2p, idx_pred_in_g2p = directed_hausdorff(gt_idx, pred_idx)

        coord_pred_in_p2g = pred_idx[idx_pred_in_p2g]
        coord_gt_in_p2g = gt_idx[idx_gt_in_p2g]
        coord_pred_in_g2p = pred_idx[idx_pred_in_g2p]
        coord_gt_in_g2p = gt_idx[idx_gt_in_g2p]

        # print(pred_idx[idx_pred_in_p2g], gt_idx[idx_gt_in_p2g])
        # print(pred_idx[idx_pred_in_g2p], gt_idx[idx_gt_in_g2p])

        # test_dist = euclidean(pred_idx[idx_pred_in_p2g], gt_idx[idx_gt_in_p2g])
        # hd_pred_to_gt_scaled = 
        coord_pred_in_p2g_scaled = coord_pred_in_p2g * spacings
        coord_gt_in_p2g_scaled = coord_gt_in_p2g * spacings
        coord_pred_in_g2p_scaled = coord_pred_in_g2p * spacings
        coord_gt_in_g2p_scaled = coord_gt_in_g2p * spacings
        # print("coord_pred_in_p2g_scaled", coord_pred_in_p2g_scaled, "coord_gt_in_p2g_scaled", coord_gt_in_p2g_scaled)
        # print("coord_pred_in_g2p_scaled", coord_pred_in_g2p_scaled, "coord_gt_in_g2p_scaled", coord_gt_in_g2p_scaled)
        # print("test_dist", )

        hd_pred_to_gt_scaled = euclidean(coord_pred_in_p2g_scaled, coord_gt_in_p2g_scaled)
        hd_gt_to_pred_scaled = euclidean(coord_pred_in_g2p_scaled, coord_gt_in_g2p_scaled)

        print("distance scaled:", hd_pred_to_gt_scaled, hd_gt_to_pred_scaled)
        hausdorff_distance = max(hd_pred_to_gt_scaled, hd_gt_to_pred_scaled)
        print("hausdorff distance {}:".format(seg), hausdorff_distance)
        hausdorff_distance_list.append(hausdorff_distance)
    avg = torch.mean(torch.Tensor(hausdorff_distance_list))
    return hausdorff_distance_list, avg



@utils.timeit
def calculate_hausdorff_distance_torch(gt, pred):
    gt = torch.from_numpy(gt)
    pred = torch.from_numpy(pred)
    gt.to("cuda")
    pred.to("cuda")
    # Find the indices of non-zero elements using PyTorch
    predicted_indices = torch.nonzero(pred == 1, as_tuple=False)
    ground_truth_indices = torch.nonzero(gt == 1, as_tuple=False)
    predicted_indices = torch.nn.functional.interpolate(predicted_indices.float().unsqueeze(0).unsqueeze(0), size=(100000,3)).squeeze()
    ground_truth_indices = torch.nn.functional.interpolate(ground_truth_indices.float().unsqueeze(0).unsqueeze(0), size=(100000, 3)).squeeze()
    ground_truth_indices.to("cuda")
    predicted_indices.to("cuda")

    # Calculate pairwise Euclidean distances between points
    def euclidean_distance(x, y):
        return torch.sqrt(torch.sum((x - y) ** 2, dim=1))

    # distances_gt_to_pred = euclidean_distance(
    #     ground_truth_indices[0].unsqueeze(1), predicted_indices[0].unsqueeze(0)
    # )
    # distances_pred_to_gt = euclidean_distance(
    #     predicted_indices[0].unsqueeze(1), ground_truth_indices[0].unsqueeze(0)
    # )

    # distances_gt_to_pred = torch.sqrt(torch.sum((ground_truth_indices[0].unsqueeze(1) - predicted_indices[0].unsqueeze(0)) ** 2, dim=1))
    # distances_pred_to_gt = torch.sqrt(torch.sum((predicted_indices[0].unsqueeze(1) - ground_truth_indices[0].unsqueeze(0)) ** 2, dim=1))
    print("ground_truth_indices.shape", ground_truth_indices.shape, "predicted_indices.shape", predicted_indices.shape)
    dist = torch.cdist(ground_truth_indices.unsqueeze(0), predicted_indices.unsqueeze(0))

    # Find the directed Hausdorff distances
    # hausdorff_distance_gt_to_pred = torch.max(torch.min(distances_gt_to_pred, dim=0).values)
    # hausdorff_distance_pred_to_gt = torch.max(torch.min(distances_pred_to_gt, dim=0).values)

    # # Get the maximum Hausdorff distance
    # overall_hausdorff_distance = torch.max(hausdorff_distance_gt_to_pred, hausdorff_distance_pred_to_gt)

    # print("Overall Hausdorff Distance:", overall_hausdorff_distance.item())
    # return overall_hausdorff_distance.item()


# functions to calculate f1 score for segmentations marked by CS
# This is for the old folder structure
# in the future, we may use the new folder structure
def calc_folder (
    folder,
    ):
    f1_score_list = {}
    for f in os.listdir(folder):    # patient
        cur_dir = os.path.join(folder, f)
        print("entering folder", cur_dir)
        for sub_dir in os.listdir(cur_dir): # left right
            print(sub_dir)
            for sub_dir_exp in os.listdir(os.path.join(cur_dir, sub_dir)):
                # print(sub_dir_exp)
                seg_file_list = os.listdir(os.path.join(cur_dir, sub_dir, sub_dir_exp))
                # print(seg_file_list)
                gt = list(filter(lambda x: "JM" in x, seg_file_list))[0]
                if len(list(filter(lambda x: "CS" in x, seg_file_list))) < 1:
                    pred = list(filter(lambda x: "SST" in x, seg_file_list))[0]
                else:
                    pred = list(filter(lambda x: "CS" in x, seg_file_list))[0]
                # f1_score = calc_f1_score(gt=gt, pred=pred)
                # f1_score_list[f] = f1_score
                print("gt", gt)
                print("pred", pred)
                gt_array = nrrd.read(os.path.join(cur_dir, sub_dir, sub_dir_exp, gt))[0]
                pred_array = nrrd.read(os.path.join(cur_dir, sub_dir, sub_dir_exp, pred))[0]
                f1_score, avg = calc_f1_score(gt=gt_array, pred=pred_array)
                f1_score_list[pred] = {"f1_score": f1_score, "avg":avg}
    print(f1_score_list)
    output_path = os.path.join(folder, "f1_score_CS.json")
    with open(output_path, "w") as f:
        f.write(json.dumps(f1_score_list))
    print("saved to", output_path)
    return f1_score_list



def test_calc_merged(
    folder,
    merged_folder,
    ):
    rater_0_folder = os.path.join(folder, "rater_0")
    rater_1_folder = os.path.join(folder, "rater_1")
    rater_2_folder = os.path.join(folder, "rater_2")

    merged_seg_list = glob.glob(os.path.join(merged_folder, "*.nrrd"))
    print(merged_seg_list)

    for merged_seg in merged_seg_list:
        merged_seg_path = os.path.join(merged_folder, merged_seg)
        seg_0_path = glob.glob(os.path.join(rater_0_folder, merged_seg.split("/")[-1][:8] + "*"))[0]
        seg_1_path = glob.glob(os.path.join(rater_1_folder, merged_seg.split("/")[-1][:8] + "*"))[0]
        seg_2_path = glob.glob(os.path.join(rater_2_folder, merged_seg.split("/")[-1][:8] + "*"))[0]
        print("seg_0_path", seg_0_path)
        print("seg_1_path", seg_1_path)
        print("seg_2_path", seg_2_path)
        print("merged_seg_path", merged_seg_path)
        f1_score_seg_0 = calc_f1_score(gt=nrrd.read(merged_seg_path)[0], pred=nrrd.read(seg_0_path)[0])
        f1_score_seg_1 = calc_f1_score(gt=nrrd.read(merged_seg_path)[0], pred=nrrd.read(seg_1_path)[0])
        f1_score_seg_2 = calc_f1_score(gt=nrrd.read(merged_seg_path)[0], pred=nrrd.read(seg_2_path)[0])
        print("f1_score_seg_0", f1_score_seg_0)
        print("f1_score_seg_1", f1_score_seg_1)
        print("f1_score_seg_2", f1_score_seg_2)





if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--gt', type=str, help='path to ground truth .nrrd file')
    parser.add_argument('--pred', type=str, help='path to prediction .nrrd file')

    args = parser.parse_args()
    gt = nrrd.read(args.gt)[0]
    pred = nrrd.read(args.pred)[0]
    
    print("gt.shape", gt.shape)
    print("pred.shape", pred.shape)


    hausdorff_distance, avg = calc_hausdorff_distance(gt, pred)
    # calculate_hausdorff_distance_torch(gt, pred)
    # print("hausdorff distance: ", hausdorff_distance)   




