import numpy as np
from scipy.interpolate import RegularGridInterpolator
import nrrd
from funcs import utils
# import xmltodict


def cart2fan(Z, X, Y):
    """
    Perform a coordinate transformation from Cartesian to fanbeam (polar) coordinates.

    This function takes Cartesian coordinates of points in 3D space and converts
    them to fanbeam (polar) coordinates, providing the radial distance and
    angular positions.

    Parameters:
    -----------
    Z : numpy.ndarray
        Array representing the Cartesian Z-coordinates of the points.

    X : numpy.ndarray
        Array representing the Cartesian X-coordinates of the points.

    Y : numpy.ndarray
        Array representing the Cartesian Y-coordinates of the points.

    Returns:
    --------
    r : numpy.ndarray
        The radial distances from the origin to each point in fanbeam coordinates.

    x_rad : numpy.ndarray
        The transformed X-coordinates in fanbeam coordinates.

    y_rad : numpy.ndarray
        The transformed Y-coordinates in fanbeam coordinates.
    """

    hypotxy = np.hypot(X, Y)
    elev = np.pi/2 - np.arctan2(Z, hypotxy)
    az = np.arctan2(Y, X)

    x_rad = elev * np.cos(az)
    y_rad = elev * np.sin(az)
    r = np.hypot(hypotxy, Z)

    return r, x_rad, y_rad



def FanDistCorr(Int, oct, method='linear', fill_value=0, *args):
    """
    Perform distortion correction on measurement data.

    This function corrects distortion in measurement data using the provided
    configuration parameters and interpolation method.

    Parameters:
    ----------
    Int : numpy.ndarray
        The measurement data to be distortion-corrected. The shape of the array
        should depend on the data dimensionality (2D or 3D) as specified in the
        'oct' dictionary.

    oct : dict
        A dictionary containing configuration parameters for distortion
        correction and measurement settings. It should include keys such as
        'Distortion_Correction_Preview', 'Scan_Settings_Measurement', and others
        as required by the function.

    method : str, optional
        The interpolation method to use during distortion correction. It
        determines how values are interpolated when sampling points are not
        on grid nodes. Default is 'linear'.

    fill_value: float or None
        The greyscale value to use for points outside of the interpolation domain
        used in RegularGridInterpolator.
        If None, values outside the domain are extrapolated.

    Returns:
    -------
    Int_corr : numpy.ndarray
        The distortion-corrected measurement data. The shape of the returned
        array matches the input 'Int'.
    """
    datatype = Int.dtype

    ref = oct['Distortion_Correction_Preview']['DC_IN_r_RefPlane_mm']
    print("ref:", ref)
    # print(1)
    x_rad = (np.linspace(
                        float(oct['Scan_Settings_Measurement']['x_min']),
                        float(oct['Scan_Settings_Measurement']['x_max']),
                        oct['Scan_Settings_Measurement']['Nx'],
                ) - oct['Distortion_Correction_Preview']['DC_IN_OpticalAxis_xPos']) * oct['Distortion_Correction_Preview']['DC_IN_ConversionFactor_x_phi'] * np.pi / 180
    print("x_rad.shape:", x_rad.shape)
    # print(2)
    y_rad = (np.linspace(oct['Scan_Settings_Measurement']['y_min'],
                        oct['Scan_Settings_Measurement']['y_max'],
                        oct['Scan_Settings_Measurement']['Ny']) -
            oct['Distortion_Correction_Preview']['DC_IN_OpticalAxis_yPos']) * \
            oct['Distortion_Correction_Preview']['DC_IN_ConversionFactor_y_phi'] * np.pi / 180
    print("y_rad.shape:", y_rad.shape)
    r_mm = np.linspace(ref, ref + oct['Distortion_Correction_Preview']['DC_IN_r_Range_mm'],
                        oct['Distortion_Correction_Preview']['DC_IN_r_N'])
    print("r_mm.shape:", r_mm.shape)

    dz = oct['Distortion_Correction_Preview']['DC_OUT_dZ_mm']
    dx = oct['Distortion_Correction_Preview']['DC_OUT_dX_mm']
    dy = oct['Distortion_Correction_Preview']['DC_OUT_dY_mm']
    z0 = oct['Distortion_Correction_Preview']['DC_OUT_Z_UpperEdge_mm']
    z_range = oct['Distortion_Correction_Preview']['DC_OUT_Z_Range_mm']
    x_range = oct['Distortion_Correction_Preview']['DC_OUT_X_Range_mm']
    y_range = oct['Distortion_Correction_Preview']['DC_OUT_Y_Range_mm']


    xt_vec = np.arange(-x_range / 2, x_range / 2 + dx, dx)
    yt_vec = np.arange(-y_range / 2, y_range / 2 + dy, dy)
    zt_vec = np.arange(z0, z0 + z_range + dz, dz)
    print("xt_vec.shape:", xt_vec.shape)
    print("yt_vec.shape:", yt_vec.shape)
    print("zt_vec.shape:", zt_vec.shape)

    # return None
    if Int.shape[2] == 1:
        print(4)
        F = RegularGridInterpolator((r_mm, x_rad), Int.astype(float), method=method, bounds_error=False, fill_value=fill_value)
        zt, xt = np.meshgrid(zt_vec, xt_vec, indexing='ij')
        rt, xt_rad, _ = cart2fan(zt, xt, 0)
        Int_corr = F((rt, xt_rad)).astype(datatype)
    else:
        print(5)
        F = RegularGridInterpolator((r_mm, x_rad, y_rad), Int.astype(float), method=method, bounds_error=False, fill_value=fill_value)
        zt, xt, yt = np.meshgrid(zt_vec, xt_vec, yt_vec, indexing='ij')
        rt, xt_rad, yt_rad = cart2fan(zt, xt, yt)
        Int_corr = F((rt, xt_rad, yt_rad)).astype(datatype)
    return Int_corr


def rotate():
    path = "../test_data/test_fanshape_correction.nrrd"
    path_output = "../test_data/test_fanshape_correction_rotated.nrrd"
    data, header = nrrd.read(path)
    data = np.transpose(data, (1, 0, 2))
    print(data.shape)
    nrrd.write(file=path_output, data=data, header=header)

