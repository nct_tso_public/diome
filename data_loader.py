import os
import glob
import nrrd
import torch
from torch.utils.data import Dataset
import yaml
import json


class OCTSample():
    def __init__(self, sample_folder) -> None:
        self.sample_folder = sample_folder
        self.idx = int(os.path.basename(sample_folder).split("_")[1])

    def load_metadata(self, metadata_path):
        with open(metadata_path, 'r') as f:
            metadata = yaml.load(f, Loader=yaml.FullLoader)
        return metadata


    def load_landmarks(self,):
        landmarks_folder = os.path.join(self.sample_folder, "annotations", "merged", "landmarks")
        landmarks_filename_list = [
            "annulus.json",
            "umbo.json",
            "short_process_of_malleus.json",
            "malleus_handle.json",
            "long_process_of_incus.json",
            "incus.json",
        ]

        landmarks_list = []
        for f in landmarks_filename_list:
            if os.path.exists(os.path.join(landmarks_folder, f)):
                print("File exists: ", os.path.join(landmarks_folder, f))
                with open(os.path.join(landmarks_folder, f), 'r') as f:
                    landmarks_json = json.load(f)
                    # landmarks_json_list.append(json.load(f))
                    landmarks_points = [ c["position"]  for c in landmarks_json["markups"][0]["controlPoints"]]
                    print(len(landmarks_points))
                landmarks_list.append(landmarks_points)

        return landmarks_list


    def load(self, ):
        print("loading sample: ", self.sample_folder)
        # load meta data from YAML file
        self.meta = self.load_metadata(os.path.join(self.sample_folder, 'meta_{}.yaml'.format(self.idx)))
        
        # load image
        image_path = os.path.join(self.sample_folder, 'OCT_{}.nrrd'.format(self.idx))
        image_data, _ = nrrd.read(image_path)

        # load segmentations
        segmentations = []
        for rater in range(0, 3):  # Assuming three raters
            segmentation_path = os.path.join(self.sample_folder, "annotations", "seg_{}.seg.nrrd".format(rater))
            segmentation_data, _ = nrrd.read(segmentation_path)
            segmentation_tensor = torch.tensor(segmentation_data, dtype=torch.float32)
            segmentations.append(segmentation_tensor)
        # segmentation merged
        segmentation_merged_path = os.path.join(self.sample_folder, "annotations", "merged", "seg_merged.seg.nrrd")
        segmentation_merged, _ = nrrd.read(segmentation_merged_path)

        # load landmarks
        landmarks_list = self.load_landmarks()

        return {
            'image': image_data,
            'segmentations': segmentations,
            "segmentation_merged": segmentation_merged,
            "landmarks": landmarks_list,
            'metadata': self.meta,
        }


class OCTDataset(Dataset):
    def __init__(self, 
                data_folder,
                num_samples=30,
            ):
        self.data_folder = data_folder
        self.num_samples = num_samples
        self.samples = []
        self.get_samples()
        print(len(self.samples))

    def __len__(self):
        return len(self.samples)

    def get_samples(self):
        for idx in range(self.num_samples):
            sample_folder = os.path.join(self.data_folder, "sample_{}".format(idx))
            self.samples.append(OCTSample(sample_folder))

    def __getitem__(self, idx):
        sample = self.samples[idx]
        res = sample.load()

        data = {
            'image': torch.tensor(res["image"]),
            'segmentations': torch.cat([ s.unsqueeze(dim=0) for s in res["segmentations"]], dim=0),
            "segmentation_merged": torch.tensor(res["segmentation_merged"]),
            "landmarks": res["landmarks"], # re-write collate_fn to land landmarks to tensor
        }
        return data


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_folder', type=str, required=True)
    parser.add_argument("--num_samples", type=int, default=30)
    args = parser.parse_args()
    data_folder = args.data_folder
    num_samples = args.num_samples
    
    dataset = OCTDataset(
        data_folder= data_folder,
        num_samples=num_samples,
    )
    print(len(dataset))

    res = dataset[0]
    print(res.keys())
    print(res["segmentations"].shape)

