# Dresden in vivo OCT Dataset of the Middle Ear

This git contains the most relevant scripts used to create the Dresden in vivo OCT dataset of the middle ear (DIOME). 

The DIOME can be downloaded from this [link](http://dx.doi.org/10.25532/OPARA-279)


## Merge segmentation masks

The segmentation masks were stored under each sample folder. Load the segmentations into list then using the function `merge` in `funcs/merge.py` to perform merging.

Two merging methods STAPLE and majority are provided.

## Multi-rater evaluation

The `evaluate_multi_raters.py` script can be used to compare the annotations of the different annotators to the final annotation by computing the F1 Score and the Hausdorff distance.

## Fan shape correction

Original OCT images are distorted in a fan shape, in order to get the physically corrcted middle ear model, *Fan Shape Correction* needs to be performed using the provided scripts:

```
python3 reshaper.py --img_path PATH_TO_OCT_IMG --yaml_path PATH_TO_YAML
```

# Data loader

example of loading samples (images, segmentations, and landmarks) as pytorch tensors:

```
python3 data_loader.py --data_folder DIOME_FOLDER --num_samples 35
```

## Credit

If you use or re-use parts of the code, please refer to our paper (to be updated).
